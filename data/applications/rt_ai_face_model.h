#ifndef __RT_AI_FACE_MODEL_H
#define __RT_AI_FACE_MODEL_H

/* model info ... */

// model name
#define RT_AI_FACE_MODEL_NAME			"face"

#define RT_AI_FACE_WORK_BUFFER_BYTES		(23856)

#define AI_FACE_DATA_WEIGHTS_SIZE		(648444)

#define RT_AI_FACE_BUFFER_ALIGNMENT		(4)

#define RT_AI_FACE_IN_NUM				(1)

#define RT_AI_FACE_IN_SIZE_BYTES	{	\
	((64 * 64 * 3) * 1),	\
}
#define RT_AI_FACE_IN_1_SIZE			(64 * 64 * 3)
#define RT_AI_FACE_IN_1_SIZE_BYTES		((64 * 64 * 3) * 1)
#define RT_AI_FACE_IN_TOTAL_SIZE_BYTES		((64 * 64 * 3) * 1)



#define RT_AI_FACE_OUT_NUM				(1)

#define RT_AI_FACE_OUT_SIZE_BYTES	{	\
	((1 * 1 * 3) * 1),	\
}
#define RT_AI_FACE_OUT_1_SIZE			(1 * 1 * 3)
#define RT_AI_FACE_OUT_1_SIZE_BYTES		((1 * 1 * 3) * 1)
#define RT_AI_FACE_OUT_TOTAL_SIZE_BYTES		((1 * 1 * 3) * 1)




#define RT_AI_FACE_TOTAL_BUFFER_SIZE		//unused

#endif	//end
